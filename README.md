# CIL Road Segmentation Project

## Additional material

The additional material can be downloaded from https://polybox.ethz.ch/index.php/s/of9KoHZL33y1lxX.
It contains the original and preprocessed data, the trained models, the predictions and submission files.

## Setup

Create virtual environment

```
python -m venv .venv
```

Activate environment (Windows)

```
.venv\Scripts\activate.bat
```

Activate environment (Unix or MacOS)

```
.venv/bin/activate
```

Installing dependencies (for CPU)

```
pip install -r requirements.cpu.txt
```

Installing dependencies (for GPU, recommended)

```
pip install -r requirements.gpu.txt
```

## Execution

The scrips were tested with Python 3.7.
All the scrips can be executed as follows

```
python -m <module name>
```

For example

```
python -m src.misc.mask_to_submission
```

The configurations related to a script are right at the beginning of each script (variables in capital letters).
Most of these variables should already be set to the right values. However, the paths have to be adjusted.

The data can be preprocessed with

```
python -m src.preprocessing.generate_regular_training_data
```

```
python -m src.preprocessing.generate_cleaning_training_data
```

Afterwards it can be split into training and validation set using

```
python -m src.preprocessing.split_regular_training_data
```

```
python -m src.preprocessing.split_cleaning_training_data
```

The models can be trained with (replace *** with the desired model name)

```
python -m src.models.train_***
```

The predictions can be computed with (replace *** with the name of the desired prediction method)

```
python -m src.models.compute_***
```

The predictions (or groundtruth) can be converted to a submission file with

```
python -m src.misc.mask_to_submission
```

The scores can be computed with

```
python -m src.misc.compute_score
```

To compute the red prediction overlays execute

```
python -m src.misc.compute_overlay
```
