import os

from keras import losses
from keras import optimizers
import numpy as np
from PIL import Image

from src.libs import image_utils
import src.unet.full_cnn_model as cnn


PATCH_SIZE = 16
PATCH_EXTENT = 1
EPOCHS = 10
BATCH_SIZE = 100

TRAINING_IMAGE_DIR = 'data/training/images'
TRAINING_IMAGE_PREFIX = 'satImage'
GT_IMAGE_DIR = 'data/training/groundtruth'
GT_IMAGE_PREFIX = 'satImage'
TEST_IMAGE_DIR = 'data/test'
TEST_IMAGE_PREFIX = 'test'
PREDICTION_TRAINING_DIR = 'out/prediction_training_00'
PREDICTION_TEST_DIR = 'out/prediction_test_00'


### load data
training_images = image_utils.load_images(TRAINING_IMAGE_DIR, TRAINING_IMAGE_PREFIX)
image_utils.scale_image_values(training_images)
training_patches = image_utils.extract_all_large_patches(training_images, PATCH_SIZE, PATCH_EXTENT)

gt_images = image_utils.load_images(GT_IMAGE_DIR, GT_IMAGE_PREFIX)
label_patches = image_utils.extract_all_patches(gt_images, PATCH_SIZE)
labels = image_utils.extract_all_patch_labels(label_patches)


### debug loading
# for i in range(0, 1000):
#     u8 = image_utils.img_float_to_uint8(training_patches[i])

#     img = Image.fromarray(u8, 'RGB')

#     file_name = 'debug_' + '%.3d' % i + '.png'
#     img.save(os.path.join('out/debug', file_name))

print(training_patches.shape)
print(labels.shape)


### class balancing
labels_maks = (labels[:, 0] == 1.0).astype(float)

pos_label_indices = np.where(labels_maks == 1.0)[0]
neg_label_indices = np.where(labels_maks == 0.0)[0]

sub_sample_indices = np.random.choice(neg_label_indices.shape[0], pos_label_indices.shape[0])
sub_neg_labels_indices = neg_label_indices[(sub_sample_indices, )]
sub_label_indices = np.concatenate((pos_label_indices, sub_neg_labels_indices))

training_patches = training_patches[(sub_label_indices, )]
labels = labels[(sub_label_indices, )]


### debug
print(training_patches.shape)
print(labels.shape)


### fit model
channel_count = 3
patch_full_size = (PATCH_EXTENT * 2 + 1) * PATCH_SIZE
input_shape = (patch_full_size, patch_full_size, channel_count)

model = cnn.FullCNNModel.build(input_shape)
optimizer = optimizers.Adam()
loss = losses.binary_crossentropy

model.compile(optimizer=optimizer, loss=loss, metrics=["accuracy"])
model.fit(training_patches, labels, epochs=EPOCHS, batch_size=BATCH_SIZE)


### prediction
test_images = image_utils.load_images(TEST_IMAGE_DIR, TEST_IMAGE_PREFIX)
image_utils.scale_image_values(test_images)
test_patches = image_utils.extract_all_large_patches(test_images, PATCH_SIZE, PATCH_EXTENT)

predicted = model.predict(test_patches)

# debug
print(np.min(np.array(predicted)[:, 1]))
print(np.mean(np.array(predicted)[:, 1]))
print(np.max(np.array(predicted)[:, 1]))
print(np.array(predicted)[:, 1].shape)

predicted = (np.array(predicted)[:, 0] > 0.5).astype(float)

test_image_count = len(test_images['id'])
test_image_patch_count_v = test_images['img'][0].shape[0] // PATCH_SIZE
test_image_patch_count_h = test_images['img'][0].shape[1] // PATCH_SIZE

predicted = np.reshape(predicted, (test_image_count, test_image_patch_count_v, test_image_patch_count_h))


### save predictions
try:
    os.makedirs(PREDICTION_TEST_DIR)
except FileExistsError:
    pass

for i in range(0, test_image_count):
    pred = predicted[i, :, :]
    pred = np.kron(pred, np.ones((PATCH_SIZE, PATCH_SIZE)))

    tmp = np.zeros((pred.shape[0], pred.shape[1], 3))
    tmp[:, :, 0] = pred
    tmp[:, :, 1] = pred
    tmp[:, :, 2] = pred

    img = Image.fromarray(image_utils.img_float_to_uint8(tmp), 'RGB')

    file_name = 'prediction_' + '%.3d' % test_images['id'][i] + '.png'
    img.save(os.path.join(PREDICTION_TEST_DIR, file_name))
