from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers import GlobalMaxPooling2D
from keras.layers import GlobalAveragePooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras import backend as K


class FullCNNModel:
    @staticmethod
    def build(input_shape):
        model = Sequential()

        model.add(Conv2D(32, (5, 5), padding="same", input_shape=input_shape))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(MaxPooling2D(pool_size=(2, 2)))
        
        model.add(Conv2D(64, (5, 5), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(Conv2D(64, (5, 5), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(MaxPooling2D(pool_size=(2, 2)))
        
        model.add(Conv2D(128, (5, 5), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(Conv2D(128, (5, 5), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(MaxPooling2D(pool_size=(2, 2)))

        # model.add(Conv2D(256, (5, 5), padding="same"))
        # model.add(Activation("relu"))
        # model.add(BatchNormalization())
        # model.add(Conv2D(256, (5, 5), padding="same"))
        # model.add(Activation("relu"))
        # model.add(BatchNormalization())
        # model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(2, (5, 5), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(GlobalAveragePooling2D())
        model.add(Dense(2))
        model.add(Activation("softmax"))

        return model