import re
import os
import itertools

import pandas as pd
import numpy as np
import matplotlib.image as mp_img


def load_images(dir, name_prefix):
    img_dir = os.path.abspath(dir)
    img_file_name_reg = re.compile(f'^{name_prefix}_([0-9]+)\.png$')

    images = list()

    for img_file_name in os.listdir(img_dir):
        match = img_file_name_reg.match(img_file_name)
        
        if not match:
            continue
        
        id = int(match.group(1))
        img = mp_img.imread(os.path.join(img_dir, img_file_name))
        
        images.append({'id': id, 'img': img})
            
    images = pd.DataFrame(images)

    return images


def scale_image_values(images):
    def exec(img):
        img = img - 0.5

        return img

    images['img'] = images['img'].apply(exec)


def extract_all_large_patches(images, patch_size, border_size):
    def exec(img):
        patch_count_0 = int(img.shape[0] / patch_size)
        patch_count_1 = int(img.shape[1] / patch_size)
        patches = list()

        full_size = border_size * 2 + 1
        patch_full_size = full_size * patch_size
        border_offset_l = border_size * patch_size
        border_offset_h = (border_size + 1) * patch_size

        for i in range(0, patch_count_0):
            for j in range(0, patch_count_1):
                patch = np.zeros((patch_full_size, patch_full_size, 3))
                
                i_p = i * patch_size
                j_p = j * patch_size

                i_l = max(i_p - border_offset_l, 0)
                i_h = min(i_p + border_offset_h, img.shape[0])
                j_l = max(j_p - border_offset_l, 0)
                j_h = min(j_p + border_offset_h, img.shape[1])

                s_i_l = max(i - border_size, 0)
                s_i_l = max(s_i_l - (i - border_size), 0) * patch_size

                s_i_h = min(i + border_size, patch_count_0 - 1)
                s_i_h = (full_size + min(s_i_h - (i + border_size), 0)) * patch_size

                s_j_l = max(j - border_size, 0)
                s_j_l = max(s_j_l - (j - border_size), 0) * patch_size

                s_j_h = min(j + border_size, patch_count_1 - 1)
                s_j_h = (full_size + min(s_j_h - (j + border_size), 0)) * patch_size

                patch[s_i_l:s_i_h , s_j_l:s_j_h] = img[i_l:i_h, j_l:j_h]

                patches.append(patch)

        return patches 
    
    patches = images['img'].apply(exec)

    return np.array(list(itertools.chain.from_iterable(patches)))


def extract_all_patches(images, patch_size):
    def exec(img):
        patches = list()

        for i in range(0, img.shape[0], patch_size):
            for j in range(0, img.shape[1], patch_size):
                patches.append(img[i:i+patch_size, j:j+patch_size])

        return patches 
    
    patches = images['img'].apply(exec)

    return np.array(list(itertools.chain.from_iterable(patches)))


def extract_all_patch_labels(patches):
    return np.array([[int(np.mean(p) > 0.5), int(np.mean(p) <= 0.5)] for p in patches])


def extract_patch_stats(images, patch_size):
    def exec(img):
        patch_count_0 = int(img.shape[0] / patch_size)
        patch_count_1 = int(img.shape[1] / patch_size)
        patches = np.zeros(shape=(patch_count_0, patch_count_1, 6))

        img_i = 0

        for i in range(0, patch_count_0):
            img_j = 0

            for j in range(0, patch_count_1):
                patch = img[img_i:img_i+patch_size, img_j:img_j+patch_size, :]
                mean = np.mean(patch, axis=(0, 1))
                var = np.var(patch, axis=(0, 1))
                
                patches[i, j, 0:3] = mean
                patches[i, j, 3:6] = var

                img_j = img_j + patch_size
            
            img_i = img_i + patch_size

        return patches

    images['stats'] = images['img'].apply(exec)


def extract_patch_labels(images, patch_size):
    def exec(img):
        patch_count_0 = int(img.shape[0] / patch_size)
        patch_count_1 = int(img.shape[1] / patch_size)
        patches = np.zeros(shape=(patch_count_0, patch_count_1))
        
        img_i = 0

        for i in range(0, patch_count_0):
            img_j = 0

            for j in range(0, patch_count_1):
                patch = img[img_i:img_i+patch_size, img_j:img_j+patch_size]
                patch_mean = np.mean(patch)
                
                patches[i, j] = int(patch_mean > 0.1)

                img_j = img_j + patch_size
            
            img_i = img_i + patch_size

        return patches

    images['labels'] = images['img'].apply(exec)


def img_float_to_uint8(img):
    rimg = img - np.min(img)
    rimg = (rimg / np.max(rimg) * 255).round().astype(np.uint8)
    return rimg