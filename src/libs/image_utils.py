import re
import os
import itertools
import math
import random

import pandas as pd
import numpy as np
import matplotlib.image as mp_img
from scipy import ndimage
from PIL import Image
from PIL import ImageDraw
from PIL import ImageEnhance
from scipy import signal
import cv2

from src.libs import utils


def load_images(dir, name_prefix):
    img_dir = os.path.abspath(dir)
    img_file_name_reg = re.compile(f'^{name_prefix}_([0-9]+)\.png$')

    images = list()

    for img_file_name in os.listdir(img_dir):
        match = img_file_name_reg.match(img_file_name)
        
        if not match:
            continue
        
        id = int(match.group(1))
        img = mp_img.imread(os.path.join(img_dir, img_file_name))
        
        images.append({'id': id, 'img': img})
            
    images = pd.DataFrame(images)
    images = images.sort_values(by=['id'])

    return images


def to_raw_array(images):
    return np.array(list(images['img']))


def extend_borders(images, border_size):
    extended_images = []

    for i in range(0, images.shape[0]):
        img = cv2.copyMakeBorder(images[i], border_size, border_size, border_size, border_size, cv2.BORDER_REPLICATE)

        extended_images.append(img)

    return np.array(extended_images)


def crop_borders(images, border_size):
    extended_images = []

    for i in range(0, images.shape[0]):
        img = images[i, border_size:-border_size, border_size:-border_size]

        extended_images.append(img)

    return np.array(extended_images)


def apply_contrast_brightness_saturation(images, contrast, brightness, saturation):
    cb_images = []

    for i in range(0, images.shape[0]):
        img = Image.fromarray(img_float_to_uint8(images[i]))

        img = ImageEnhance.Color(img).enhance(saturation)
        img = ImageEnhance.Brightness(img).enhance(brightness)
        img = ImageEnhance.Contrast(img).enhance(contrast)

        img = np.array(img)
        img = img_uint8_to_float_non_norm(img)

        cb_images.append(img)

    return np.array(cb_images)


def apply_flip(images, axes):
    flipped_images = []

    for i in range(0, images.shape[0]):
        img = images[i]

        if axes[0]:
            img = np.flip(img, axis=0)

        if axes[1]:
            img = np.flip(img, axis=1)

        flipped_images.append(img)

    return np.array(flipped_images)


def apply_average_conv(images):
    conv_images = []

    kernel = np.ones((5, 40))

    for i in range(0, images.shape[0]):
        img = np.zeros((images.shape[1], images.shape[2], 3))
        img[:, :, 0] = signal.convolve2d(images[i, :, :, 0], kernel, boundary='symm', mode='same')
        img[:, :, 1] = signal.convolve2d(images[i, :, :, 1], kernel, boundary='symm', mode='same')
        img[:, :, 2] = signal.convolve2d(images[i, :, :, 2], kernel, boundary='symm', mode='same')

        conv_images.append(img)

    return np.array(conv_images)


def extract_tiled_patches(images, patch_size):
    extracted_patches = []

    for i in range(0, images.shape[0]):
        for j in range(0, images.shape[1], patch_size):
            for w in range(0, images.shape[2], patch_size):
                extracted_patches.append(images[i, j:j+patch_size, w:w+patch_size])

    return np.array(extracted_patches)


def extract_edges(images):
    edge_images = []

    for i in range(0, images.shape[0]):
        img_x_0 = ndimage.sobel(images[i, :, :, 0], axis=0)
        img_y_0 = ndimage.sobel(images[i, :, :, 0], axis=1)
        img_x_1 = ndimage.sobel(images[i, :, :, 1], axis=0)
        img_y_1 = ndimage.sobel(images[i, :, :, 1], axis=1)
        img_x_2 = ndimage.sobel(images[i, :, :, 2], axis=0)
        img_y_2 = ndimage.sobel(images[i, :, :, 2], axis=1)

        img_0 = np.hypot(img_x_0, img_y_0)
        img_1 = np.hypot(img_x_1, img_y_1)
        img_2 = np.hypot(img_x_2, img_y_2)
        img = np.log((((img_0 + img_1 + img_2) / 3) + 1) * 100)

        edge_images.append(img)

    return np.array(edge_images)


def apply_random_dilation(images):
    dilated_images = []

    for i in range(0, images.shape[0]):
        size = random.randint(1, 5)
        size = (size, size)

        img = ndimage.morphology.grey_dilation(images[i], size=size, structure=np.ones(size))

        dilated_images.append(img)

    return np.array(dilated_images)


def draw_ellipse(image, position, size, rotation, color):
    overlay = Image.new('RGBA', size)
    overlay_draw = ImageDraw.Draw(overlay)
    overlay_draw.ellipse([(0, 0), size], color)

    overlay = overlay.rotate(rotation, expand=True)

    image.paste(overlay, position, overlay)


def draw_box(image, position, size, rotation, color):
    overlay = Image.new('RGBA', size)
    overlay_draw = ImageDraw.Draw(overlay)
    overlay_draw.rectangle([(0, 0), size], color)

    overlay = overlay.rotate(rotation, expand=True)

    image.paste(overlay, position, overlay)


def apply_random_gt_errors(images):
    error_images = []

    for i in range(0, images.shape[0]):
        img = Image.fromarray(img_float_to_uint8(images[i]))

        # white boxes

        white_box_count_vertical = random.randint(0, 1)
        white_box_count_horizontal = random.randint(0, 1)

        for j in range(0, white_box_count_vertical):
            pos_x = random.randint(0, images.shape[1] - 1)
            pos_y = random.randint(0, images.shape[2] - 1)

            size_x = random.randint(5, 100)
            size_y = random.randint(5, 25)

            rotation = random.randint(-8, 8)

            draw_box(img, (pos_x, pos_y), (size_x, size_y), rotation, '#fff')

        for j in range(0, white_box_count_horizontal):
            pos_x = random.randint(0, images.shape[1] - 1)
            pos_y = random.randint(0, images.shape[2] - 1)

            size_x = random.randint(5, 25)
            size_y = random.randint(5, 100)

            rotation = random.randint(-8, 8)

            draw_box(img, (pos_x, pos_y), (size_x, size_y), rotation, '#fff')

        # small white patches

        for j in range(0, 20):
            pos_x = random.randint(0, images.shape[1] - 1)
            pos_y = random.randint(0, images.shape[2] - 1)

            size_x = random.randint(2, 15)
            size_y = random.randint(2, 15)

            rotation = random.randint(0, 180)

            draw_ellipse(img, (pos_x, pos_y), (size_x, size_y), rotation, '#fff')

        # very small black patches

        for j in range(0, 150):
            pos_x = random.randint(0, images.shape[1] - 1)
            pos_y = random.randint(0, images.shape[2] - 1)

            size_x = random.randint(2, 15)
            size_y = random.randint(2, 5)

            rotation = random.randint(0, 180)

            draw_ellipse(img, (pos_x, pos_y), (size_x, size_y), rotation, '#000')

        # small black patches

        for j in range(0, 15):
            pos_x = random.randint(0, images.shape[1] - 1)
            pos_y = random.randint(0, images.shape[2] - 1)

            size_x = random.randint(7, 25)
            size_y = random.randint(5, 7)

            for w in range(0, 20):
                pos_x_2 = random.randint(pos_x - size_x, pos_x + size_x)
                pos_y_2 = random.randint(pos_y - size_y, pos_y + size_y)

                size_x_2 = random.randint(7, 15)
                size_y_2 = random.randint(2, 7)

                rotation = random.randint(0, 180)

                draw_ellipse(img, (pos_x_2, pos_y_2), (size_x_2, size_y_2), rotation, '#000')

            for w in range(0, 20):
                pos_x_2 = random.randint(pos_x - size_x, pos_x + size_x)
                pos_y_2 = random.randint(pos_y - size_y, pos_y + size_y)

                size_x_2 = random.randint(2, 7)
                size_y_2 = random.randint(7, 15)

                rotation = random.randint(0, 180)

                draw_ellipse(img, (pos_x_2, pos_y_2), (size_x_2, size_y_2), rotation, '#000')

        for j in range(0, 15):
            pos_x = random.randint(0, images.shape[1] - 1)
            pos_y = random.randint(0, images.shape[2] - 1)

            size_x = random.randint(5, 7)
            size_y = random.randint(7, 25)

            for w in range(0, 20):
                pos_x_2 = random.randint(pos_x - size_x, pos_x + size_x)
                pos_y_2 = random.randint(pos_y - size_y, pos_y + size_y)

                size_x_2 = random.randint(7, 15)
                size_y_2 = random.randint(2, 7)

                rotation = random.randint(0, 180)

                draw_ellipse(img, (pos_x_2, pos_y_2), (size_x_2, size_y_2), rotation, '#000')

            for w in range(0, 20):
                pos_x_2 = random.randint(pos_x - size_x, pos_x + size_x)
                pos_y_2 = random.randint(pos_y - size_y, pos_y + size_y)

                size_x_2 = random.randint(2, 7)
                size_y_2 = random.randint(7, 15)

                rotation = random.randint(0, 180)

                draw_ellipse(img, (pos_x_2, pos_y_2), (size_x_2, size_y_2), rotation, '#000')

        # large black patches

        for j in range(0, 5):
            pos_x = random.randint(0, images.shape[1] - 1)
            pos_y = random.randint(0, images.shape[2] - 1)

            size_x = random.randint(15, 40)
            size_y = random.randint(5, 7)

            for w in range(0, 50):
                pos_x_2 = random.randint(pos_x - size_x, pos_x + size_x)
                pos_y_2 = random.randint(pos_y - size_y, pos_y + size_y)

                size_x_2 = random.randint(7, 25)
                size_y_2 = random.randint(2, 10)

                rotation = random.randint(0, 180)

                draw_ellipse(img, (pos_x_2, pos_y_2), (size_x_2, size_y_2), rotation, '#000')

            for w in range(0, 50):
                pos_x_2 = random.randint(pos_x - size_x, pos_x + size_x)
                pos_y_2 = random.randint(pos_y - size_y, pos_y + size_y)

                size_x_2 = random.randint(2, 10)
                size_y_2 = random.randint(7, 25)

                rotation = random.randint(0, 180)

                draw_ellipse(img, (pos_x_2, pos_y_2), (size_x_2, size_y_2), rotation, '#000')

        for j in range(0, 5):
            pos_x = random.randint(0, images.shape[1] - 1)
            pos_y = random.randint(0, images.shape[2] - 1)

            size_x = random.randint(5, 7)
            size_y = random.randint(15, 40)

            for w in range(0, 50):
                pos_x_2 = random.randint(pos_x - size_x, pos_x + size_x)
                pos_y_2 = random.randint(pos_y - size_y, pos_y + size_y)

                size_x_2 = random.randint(7, 25)
                size_y_2 = random.randint(2, 10)

                rotation = random.randint(0, 180)

                draw_ellipse(img, (pos_x_2, pos_y_2), (size_x_2, size_y_2), rotation, '#000')

            for w in range(0, 50):
                pos_x_2 = random.randint(pos_x - size_x, pos_x + size_x)
                pos_y_2 = random.randint(pos_y - size_y, pos_y + size_y)

                size_x_2 = random.randint(2, 10)
                size_y_2 = random.randint(7, 25)

                rotation = random.randint(0, 180)

                draw_ellipse(img, (pos_x_2, pos_y_2), (size_x_2, size_y_2), rotation, '#000')

        img = np.array(img)
        img = img_uint8_to_float_non_norm(img)

        error_images.append(img)

    return np.array(error_images) 

    
def rotate_90(images, count):
    rotated_images = []

    for i in range(0, images.shape[0]):
        img = images[i]

        for j in range(0, count):
            img = np.rot90(img)

        rotated_images.append(img)

    return np.array(rotated_images)


def mark_green(images):
    marked_images = []

    for i in range(0, images.shape[0]):
        diff_r = images[i, :, :, 1] - images[i, :, :, 0]
        diff_b = images[i, :, :, 1] - images[i, :, :, 2]

        img = np.logical_and(diff_r > 0.02, diff_b > 0.02).astype(float)

        marked_images.append(img)

    return np.array(marked_images)


def apply_random_brightness(
    training_images, gt_images, brightness_change_range):
    images = []

    for i in range(0, training_images.shape[0]):
        brightness_change = utils.halton_seq(i, 7) * \
            (brightness_change_range[1] - brightness_change_range[0]) + \
            brightness_change_range[0]
        mask = np.tile(np.expand_dims(gt_images[i], -1), (1, 1, 3))

        image = training_images[i] + mask * brightness_change
        image = np.clip(image, 0.0, 1.0)

        images.append(image)

    return np.array(images)


def resize(images, target_size, antialias):
    resized_images = []

    for i in range(0, images.shape[0]):
        img = Image.fromarray(img_float_to_uint8(images[i]))

        if antialias:
            resized_image = np.array(img.resize((target_size, target_size), Image.ANTIALIAS))
        else:
            resized_image = np.array(img.resize((target_size, target_size)))

        resized_images.append(img_uint8_to_float_non_norm(resized_image))

    return np.array(resized_images)


def apply_median(images, filter_size):
    median_images = []

    for i in range(0, images.shape[0]):
        median_image = np.zeros((images.shape[1], images.shape[2], 3))
        median_image[:, :, 0] = ndimage.median_filter(images[i, :, :, 0], size=filter_size, mode='nearest')
        median_image[:, :, 1] = ndimage.median_filter(images[i, :, :, 1], size=filter_size, mode='nearest')
        median_image[:, :, 2] = ndimage.median_filter(images[i, :, :, 2], size=filter_size, mode='nearest')

        median_images.append(median_image)

    return np.array(median_images) 


def apply_gaussian(images, sigma):
    gaussian_images = []

    for i in range(0, images.shape[0]):
        gaussian_image = ndimage.gaussian_filter(images[i], sigma=sigma, mode='nearest')

        gaussian_images.append(gaussian_image)

    return np.array(gaussian_images) 


def extract_random_patches(
    images, normal_patch_count, rotation_patch_count,
    patch_size, base_rotation, max_rotation_jitter, rotation_order, offset_index):
    patches = []

    for i in range(0, images.shape[0]):
        image_height = images.shape[1]
        image_width = images.shape[2]

        sampling_area = (image_height - patch_size, image_width - patch_size)

        j_start = 0
        j_end = normal_patch_count

        for j in range(j_start, j_end):
            sampling_pos = utils.halton_seq_2d(j + offset_index, (2, 3))
            patch_start = utils.tuple_mul(sampling_pos, sampling_area)
            patch_start = utils.tuple_cast_int(patch_start)
            patch_end = utils.tuple_add(patch_start, (patch_size, patch_size))

            patch = images[i, patch_start[0]:patch_end[0], patch_start[1]:patch_end[1]]

            patches.append(patch)

        rot_patch_size = int(math.sqrt(2 * patch_size * patch_size)) + 1
        sub_patch_offset = int((rot_patch_size - patch_size) / 2)
        rot_sampling_area = (image_height - rot_patch_size, image_width - rot_patch_size)

        j_start = j_end
        j_end = j_end + rotation_patch_count

        for j in range(j_start, j_end):
            sampling_pos = utils.halton_seq_2d(j + offset_index, (2, 3))
            patch_start = utils.tuple_mul(sampling_pos, rot_sampling_area)
            patch_start = utils.tuple_cast_int(patch_start)
            patch_end = utils.tuple_add(patch_start, (rot_patch_size, rot_patch_size))

            patch = images[i, patch_start[0]:patch_end[0], patch_start[1]:patch_end[1]]

            rotation_jitter = utils.halton_seq(j, 5) * max_rotation_jitter * 2 - max_rotation_jitter
            rotation = base_rotation + rotation_jitter

            patch = ndimage.rotate(patch, rotation, reshape=False, order=rotation_order)
            patch = patch[sub_patch_offset:sub_patch_offset+patch_size, sub_patch_offset:sub_patch_offset+patch_size]

            patches.append(patch)

    return np.array(patches)


def img_float_to_uint8(img):
    rimg = img - np.min(img)
    rimg = (rimg / np.max(rimg) * 255).round().astype(np.uint8)
    return rimg


def img_float_to_uint8_non_norm(img):
    rimg = (img * 255).round().astype(np.uint8)
    return rimg


def img_uint8_to_float_non_norm(img):
    rimg = img.astype(np.float32) / 255
    return rimg


def store_images(out_dir, image_prefix, images, mode, ids=None):
    try:
        os.makedirs(out_dir)
    except FileExistsError:
        pass

    for i in range(0, images.shape[0]):
        img = Image.fromarray(img_float_to_uint8(images[i]), mode)
        
        if ids is None:
            file_name = image_prefix + '_' + '%.3d' % i + '.png'
        else:
            file_name = image_prefix + '_' + '%.3d' % ids[i] + '.png'

        img.save(os.path.join(out_dir, file_name))
