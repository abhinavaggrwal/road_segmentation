
def halton_seq_2d(i, b):
    return (halton_seq(i, b[0]), halton_seq(i, b[1]))


def halton_seq(i, b):
    f = 1
    r = 0

    while i > 0:
        f = f / b
        r = r + f * (i % b)
        i = int(i / b)

    return r


def tuple_cast_int(t):
    return tuple(int(v) for v in t)


def tuple_add(t_0, t_1):
    return tuple(v_0 + v_1 for v_0, v_1 in zip(t_0, t_1))


def tuple_mul(t_0, t_1):
    return tuple(v_0 * v_1 for v_0, v_1 in zip(t_0, t_1))
