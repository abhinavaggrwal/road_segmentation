import cv2
import numpy as np
import os
import re
from tqdm import tqdm

def load_images(dir, name_prefix, gt=False):

    img_dir = os.path.abspath(dir)
    img_file_name_reg = re.compile(f'^{name_prefix}_([0-9]+)\.png$')

    images = list()

    for img_file_name in tqdm(os.listdir(img_dir)):
        match = img_file_name_reg.match(img_file_name)
        
        if not match:
            continue
        
        # id = int(match.group(1))
        if gt:
            grey_img = cv2.imread(os.path.join(img_dir, img_file_name), 0)
            resized = cv2.resize(grey_img, (224,224), interpolation = cv2.INTER_AREA)
            resized = np.where(resized>0,1,0)
            # grey_img = np.where(grey_img>0,1,0)
            # img_compl = np.absolute(grey_img-1)
            # img = np.stack((grey_img, img_compl), axis=-1)
        else:
            img = cv2.imread(os.path.join(img_dir, img_file_name))
            resized = cv2.resize(img, (224,224), interpolation = cv2.INTER_AREA)
        
        # images.append(img)
        images.append(resized)
    
    return np.array(images)


def channel_mean(image_arr):
    assert len(image_arr.shape) == 4

    channel_mean = []
    for i in range(image_arr.shape[-1]):
        channel_mean.append(np.mean(image_arr[:,:,:,i]))

    return channel_mean


def channel_std(image_arr):
    assert len(image_arr.shape) == 4

    channel_std = []
    for i in range(image_arr.shape[-1]):
        channel_std.append(np.std(image_arr[:,:,:,i]))

    return channel_std


def normalize(image_arr):
    assert len(image_arr.shape) == 4

    means = channel_mean(image_arr)
    stds = channel_mean(image_arr)

    for i in range(image_arr.shape[-1]):
        image_arr[:,:,:,i] = (image_arr[:,:,:,i] - means[i])/stds[i]

    return image_arr


#%%
img_dir = os.path.abspath('../../data/training/groundtruth')
img = cv2.imread(os.path.join(img_dir, os.listdir(img_dir)[0]), 0)

#%%
