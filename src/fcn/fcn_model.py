import tensorflow as tf



class Models(object):
    def __init__(self, shape, seed, mode, reuse):
        # self.batchsize = batchsize
        self.h = shape[1]
        self.w = shape[2]

        self.reuse = reuse
        self.mode = mode
        self.is_training = mode=='training'
        self.seed = seed


    def create_placeholders(self):
        print('===== Creating Placeholders =====')
        self.x = tf.placeholder(dtype=tf.float32, shape=(None, self.h, self.w, 3))
        if self.mode!='testing':
            self.y = tf.placeholder(dtype=tf.int64, shape=(None, self.h, self.w))

    def initialize_weights(self):
        print('===== Creating Conv Weights =====')

        with tf.variable_scope('conv_weights', reuse=self.reuse):
            self.conv1_weights = tf.Variable(tf.truncated_normal([3, 3, 3, 64], stddev=0.1, seed=self.seed))
            self.conv1_biases = tf.Variable(tf.zeros([64]))
            self.conv2_weights = tf.Variable(tf.truncated_normal([3, 3, 64, 64], stddev=0.1, seed=self.seed))
            self.conv2_biases = tf.Variable(tf.zeros([64]))
            self.conv3_weights = tf.Variable(tf.truncated_normal([3, 3, 64, 128], stddev=0.1, seed=self.seed))
            self.conv3_biases = tf.Variable(tf.zeros([128]))
            self.conv4_weights = tf.Variable(tf.truncated_normal([3, 3, 128, 128], stddev=0.1, seed=self.seed))
            self.conv4_biases = tf.Variable(tf.zeros([128]))
            self.conv5_weights = tf.Variable(tf.truncated_normal([3, 3, 128, 256], stddev=0.1, seed=self.seed))
            self.conv5_biases = tf.Variable(tf.zeros([256]))
            self.conv6_weights = tf.Variable(tf.truncated_normal([3, 3, 256, 256], stddev=0.1, seed=self.seed))
            self.conv6_biases = tf.Variable(tf.zeros([256]))
            self.conv7_weights = tf.Variable(tf.truncated_normal([3, 3, 256, 256], stddev=0.1, seed=self.seed))
            self.conv7_biases = tf.Variable(tf.zeros([256]))
            self.conv8_weights = tf.Variable(tf.truncated_normal([3, 3, 256, 512], stddev=0.1, seed=self.seed))
            self.conv8_biases = tf.Variable(tf.zeros([512]))
            self.conv9_weights = tf.Variable(tf.truncated_normal([3, 3, 512, 512], stddev=0.1, seed=self.seed))
            self.conv9_biases = tf.Variable(tf.zeros([512]))
            self.conv10_weights = tf.Variable(tf.truncated_normal([3, 3, 512, 512], stddev=0.1, seed=self.seed))
            self.conv10_biases = tf.Variable(tf.zeros([512]))
            self.conv11_weights = tf.Variable(tf.truncated_normal([3, 3, 512, 512], stddev=0.1, seed=self.seed))
            self.conv11_biases = tf.Variable(tf.zeros([512]))
            self.conv12_weights = tf.Variable(tf.truncated_normal([3, 3, 512, 512], stddev=0.1, seed=self.seed))
            self.conv12_biases = tf.Variable(tf.zeros([512]))
            self.conv13_weights = tf.Variable(tf.truncated_normal([3, 3, 512, 512], stddev=0.1, seed=self.seed))
            self.conv13_biases = tf.Variable(tf.zeros([512]))
            self.conv14_weights = tf.Variable(tf.truncated_normal([7, 7, 512, 4096], stddev=0.1, seed=self.seed))
            self.conv14_biases = tf.Variable(tf.zeros([4096]))
            self.conv15_weights = tf.Variable(tf.truncated_normal([1, 1, 4096, 4096], stddev=0.1, seed=self.seed))
            self.conv15_biases = tf.Variable(tf.zeros([4096]))
            self.conv16_weights = tf.Variable(tf.truncated_normal([1, 1, 4096, 2], stddev=0.1, seed=self.seed))
            self.conv16_biases = tf.Variable(tf.zeros([2]))

            self.deconv_weights = tf.Variable(tf.truncated_normal([self.h, self.w, 2, 2], stddev=0.1, seed=self.seed))


    def initialize_weights1(self):
        print('===== Creating Conv Weights =====')

        with tf.variable_scope('conv_weights', reuse=self.reuse):
            self.conv1_weights = tf.get_variable('conv1_w',[3, 3, 3, 64])
            self.conv1_biases = tf.get_variable('conv1_b',[64])
            self.conv2_weights = tf.get_variable('conv2_w',[3, 3, 64, 64])
            self.conv2_biases = tf.get_variable('conv2_b',[64])
            self.conv3_weights = tf.get_variable('conv3_w',[3, 3, 64, 128])
            self.conv3_biases = tf.get_variable('conv3_b',[128])
            self.conv4_weights = tf.get_variable('conv4_w',[3, 3, 128, 128])
            self.conv4_biases = tf.get_variable('conv4_b',[128])
            self.conv5_weights = tf.get_variable('conv5_w',[3, 3, 128, 256])
            self.conv5_biases = tf.get_variable('conv5_b',[256])
            self.conv6_weights = tf.get_variable('conv6_w',[3, 3, 256, 256])
            self.conv6_biases = tf.get_variable('conv6_b',[256])
            self.conv7_weights = tf.get_variable('conv7_w',[3, 3, 256, 256])
            self.conv7_biases = tf.get_variable('conv7_b',[256])
            self.conv8_weights = tf.get_variable('conv8_w',[3, 3, 256, 512])
            self.conv8_biases = tf.get_variable('conv8_b',[512])
            self.conv9_weights = tf.get_variable('conv9_w',[3, 3, 512, 512])
            self.conv9_biases = tf.get_variable('conv9_b',[512])
            self.conv10_weights = tf.get_variable('conv10_w',[3, 3, 512, 512])
            self.conv10_biases = tf.get_variable('conv10_b',[512])
            self.conv11_weights = tf.get_variable('conv11_w',[3, 3, 512, 512])
            self.conv11_biases = tf.get_variable('conv11_b',[512])
            self.conv12_weights = tf.get_variable('conv12_w',[3, 3, 512, 512])
            self.conv12_biases = tf.get_variable('conv12_b',[512])
            self.conv13_weights = tf.get_variable('conv13_w',[3, 3, 512, 512])
            self.conv13_biases = tf.get_variable('conv13_b',[512])
            self.conv14_weights = tf.get_variable('conv14_w',[7, 7, 512, 4096])
            self.conv14_biases = tf.get_variable('conv14_b',[4096])
            self.conv15_weights = tf.get_variable('conv15_w',[1, 1, 4096, 4096])
            self.conv15_biases = tf.get_variable('conv15_b',[4096])
            self.conv16_weights = tf.get_variable('conv16_w',[1, 1, 4096, 2])
            self.conv16_biases = tf.get_variable('conv16_b',[2])

            self.deconv_weights = tf.get_variable('deconv_w',[self.h, self.w, 2, 2])



    def create_graph(self):
        print('===== Creating FCN graph =====')

        conv1 = tf.nn.conv2d(self.x, self.conv1_weights, strides=[1,1,1,1], padding='SAME')
        relu1 = tf.nn.relu(tf.nn.bias_add(conv1, self.conv1_biases  ))

        conv2 = tf.nn.conv2d(relu1, self.conv2_weights, strides=[1,1,1,1], padding='SAME')
        relu2 = tf.nn.relu(tf.nn.bias_add(conv2, self.conv2_biases  ))
        pool2 = tf.nn.max_pool(relu2, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

        conv3 = tf.nn.conv2d(pool2, self.conv3_weights, strides=[1,1,1,1], padding='SAME')
        relu3 = tf.nn.relu(tf.nn.bias_add(conv3, self.conv3_biases))

        conv4 = tf.nn.conv2d(relu3, self.conv4_weights, strides=[1,1,1,1], padding='SAME')
        relu4 = tf.nn.relu(tf.nn.bias_add(conv4, self.conv4_biases))
        pool4 = tf.nn.max_pool(relu4, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

        conv5 = tf.nn.conv2d(pool4, self.conv5_weights, strides=[1,1,1,1], padding='SAME')
        relu5 = tf.nn.relu(tf.nn.bias_add(conv5, self.conv5_biases)) 

        conv6 = tf.nn.conv2d(relu5, self.conv6_weights, strides=[1,1,1,1], padding='SAME')
        relu6 = tf.nn.relu(tf.nn.bias_add(conv6, self.conv6_biases))

        conv7 = tf.nn.conv2d(relu6, self.conv7_weights, strides=[1,1,1,1], padding='SAME')
        relu7 = tf.nn.relu(tf.nn.bias_add(conv7, self.conv7_biases))  
        pool7 = tf.nn.max_pool(relu7, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

        conv8 = tf.nn.conv2d(pool7, self.conv8_weights, strides=[1,1,1,1], padding='SAME')
        relu8 = tf.nn.relu(tf.nn.bias_add(conv8, self.conv8_biases))     

        conv9 = tf.nn.conv2d(relu8, self.conv9_weights, strides=[1,1,1,1], padding='SAME')
        relu9 = tf.nn.relu(tf.nn.bias_add(conv9, self.conv9_biases))

        conv10 = tf.nn.conv2d(relu9, self.conv10_weights, strides=[1,1,1,1], padding='SAME')
        relu10 = tf.nn.relu(tf.nn.bias_add(conv10, self.conv10_biases))
        pool10 = tf.nn.max_pool(relu10, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

        conv11 = tf.nn.conv2d(pool10, self.conv11_weights, strides=[1,1,1,1], padding='SAME')
        relu11 = tf.nn.relu(tf.nn.bias_add(conv11, self.conv11_biases))     

        conv12 = tf.nn.conv2d(relu11, self.conv12_weights, strides=[1,1,1,1], padding='SAME')
        relu12 = tf.nn.relu(tf.nn.bias_add(conv12, self.conv12_biases))

        conv13 = tf.nn.conv2d(relu12, self.conv13_weights, strides=[1,1,1,1], padding='SAME')
        relu13 = tf.nn.relu(tf.nn.bias_add(conv13, self.conv13_biases))
        pool13 = tf.nn.max_pool(relu13, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')

        conv14 = tf.nn.conv2d(pool13, self.conv14_weights, strides=[1,1,1,1], padding='VALID')
        relu14 = tf.nn.relu(tf.nn.bias_add(conv14, self.conv14_biases))     

        conv15 = tf.nn.conv2d(relu14, self.conv15_weights, strides=[1,1,1,1], padding='SAME')
        relu15 = tf.nn.relu(tf.nn.bias_add(conv15, self.conv15_biases))

        conv16 = tf.nn.conv2d(relu15, self.conv16_weights, strides=[1,1,1,1], padding='SAME')
        relu16 = tf.nn.relu(tf.nn.bias_add(conv16, self.conv16_biases))
        print(relu16)

        deconv = tf.layers.conv2d_transpose(relu16, 2, [self.h, self.w], [self.h, self.w], padding='same', reuse=self.reuse)
        
        # deconv = tf.nn.conv2d_transpose(relu16, self.deconv_weights, [tf.shape(self.x)[0], self.h, self.w, 2], strides=[1, 32, 32, 1])
        print(deconv)

        self.deconv = deconv

    def build_loss(self):
        print('===== Building Loss =====')

        # logits = tf.reshape(self.conv, [-1,2])
        # labels = tf.reshape(self.y, [-1])
        # self.pred = tf.nn.softmax(self.deconv)

        self.loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.y,logits=self.deconv))
        # print(self.loss)
        

    def optimization_routines(self, learning_rate):
        print('===== Optimization =====')
        optimizer = tf.train.AdamOptimizer(learning_rate)
        self.train_op = optimizer.minimize(self.loss)

    def build_network(self):
        self.create_placeholders()
        self.initialize_weights1()
        self.create_graph()
        self.build_loss()

    def step(self, session, feed_dict):
        
        feed_dict_batch = {self.x: feed_dict['x'], self.y: feed_dict['y']}
        if self.is_training:
            outputs = session.run([self.loss, self.train_op], feed_dict=feed_dict_batch)
        else:
            outputs = session.run([self.loss], feed_dict=feed_dict_batch)

        return outputs[0]