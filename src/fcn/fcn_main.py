#%%
import argparse
import numpy as np
import cv2
import tensorflow as tf
import os
import fcn_data as utils
from sklearn.model_selection import train_test_split
from fcn_model import Models as model_cls

parser  = argparse.ArgumentParser()

#%%
parser.add_argument('--batch_size', type=int, default=1, help='Batch Size to use for training')
parser.add_argument('--val_fraction', type=float, default=0.2, help='Fraction of total Data as Validation Data')

parser.add_argument("--num_epochs", type=int, default=51, help="Number of training epochs.")
parser.add_argument('--learning_rate', type=float, default=0.001, help='Learning Rate')

parser.add_argument("--test_every", type=int, default=1, help="After How many epochs to compute the error on the validation set.")


ARGS = parser.parse_args()


BATCHSIZE = ARGS.batch_size
VALSIZE = ARGS.val_fraction
SEED = 1992
NEPOCHS = ARGS.num_epochs

#%%
print('===== Loading Images =====')
dir_img_actual = '../../data/training/images'
dir_img_trans = '../../data/training/trans_images'
dir_gt_actual = '../../data/training/groundtruth'
dir_gt_trans = '../../data/training/trans_gt'


# images = []
actual_images = utils.load_images(dir_img_actual, 'satImage')
print(actual_images.shape)
# rot45_images = utils.load_images(dir_img_trans, 'rot45_satImage')
# rot90_images = utils.load_images(dir_img_trans, 'rot90_satImage')
# images = np.concatenate((actual_images, rot45_images, rot90_images))

gt_images = utils.load_images(dir_gt_actual, 'satImage', gt=True)
# gt_rot45 = utils.load_images(dir_gt_trans, 'rot45_satImage', gt=True)
# gt_rot90 = utils.load_images(dir_gt_trans, 'rot90_satImage', gt=True)
# gt = np.concatenate((gt_images, gt_rot45, gt_rot90))


#%%
normalized_images = utils.normalize(actual_images)
print(normalized_images.shape)
# normalized_images = utils.normalize(images)
train_img, val_img, train_gt, val_gt  = train_test_split(normalized_images, gt_images, test_size=VALSIZE, random_state=SEED)
print(train_img.shape)
print('===== Loading Complete =====')


def shuffler_and_batcher(input_data_x, input_data_y, n_splits, shuffle=False):

    if shuffle:
        indices = np.arange(input_data_x.shape[0])
        np.random.seed(SEED)
        np.random.shuffle(indices)
        input_data_x = input_data_x[indices]
        input_data_y = input_data_y[indices]

        
    batched_data_x = np.array_split(input_data_x, n_splits, axis=0)
    batched_data_y = np.array_split(input_data_y, n_splits, axis=0)

    return batched_data_x, batched_data_y

def train():
    train_model = model_cls(shape=actual_images.shape, seed=SEED, mode='training', reuse=False)
    valid_model = model_cls(shape=actual_images.shape, seed=SEED, mode='validation', reuse=True)
    train_model.build_network()
    valid_model.build_network()

    train_model.optimization_routines(ARGS.learning_rate)
    print(tf.trainable_variables())

    # train_data_num = train_img.shape[0]
    # n_batches_train = train_data_num//BATCHSIZE if train_data_num%BATCHSIZE == 0 else train_data_num//BATCHSIZE+1
    # train_data_x, train_data_y = shuffler_and_batcher(input_data_x=train_img, input_data_y=train_gt, n_splits=n_batches_train, shuffle=True)
    # print(train_data_x[0].shape, train_data_y[0].shape)
    # train_model.create_placeholders()
    # train_model.initialize_weights()
    # train_model.create_graph()
    # train_model.build_loss()

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    for idx, epoch in enumerate(range(NEPOCHS)):

        train_data_num = train_img.shape[0]
        n_batches_train = train_data_num//BATCHSIZE if train_data_num%BATCHSIZE == 0 else train_data_num//BATCHSIZE+1
        train_data_x, train_data_y = shuffler_and_batcher(input_data_x=train_img, input_data_y=train_gt, n_splits=n_batches_train, shuffle=True)
        
        total_loss_train = 0
        
        for i in range(0, n_batches_train):
            
            feed_dict = {'x':train_data_x[i], 'y':train_data_y[i] }
            train_loss = train_model.step(session=sess, feed_dict=feed_dict)
            
            total_loss_train += train_loss/n_batches_train
			# final_accuracy_train += train_accuracy/n_batches_train

		# print(total_loss_train, final_accuracy_train)

        if idx%ARGS.test_every==0:

            valid_data_num = val_img.shape[0]
            n_batches_valid = valid_data_num//BATCHSIZE if valid_data_num%BATCHSIZE == 0 else valid_data_num//BATCHSIZE+1
            valid_data_x, valid_data_y = shuffler_and_batcher(input_data_x=val_img, input_data_y=val_gt, n_splits=n_batches_valid)
            
            total_loss_valid = 0
			# final_accuracy_valid = 0

            for j in range(0, n_batches_valid):

                feed_dict = {'x':valid_data_x[j], 'y':valid_data_y[j] }
                valid_loss = valid_model.step(session=sess, feed_dict=feed_dict)
                
                total_loss_valid += valid_loss/n_batches_valid
				# final_accuracy_valid += valid_accuracy/n_batches_valid

			# print('Epoch {}: Train Loss - {:.4f}, Train Accuracy - {:.4f}, Valid Loss - {:.4f}, Valid Accuracy - {:.4f}'.format(idx+1, total_loss_train, final_accuracy_train, total_loss_valid, final_accuracy_valid))
            print('Epoch {}: Train Loss - {:.4f}, Valid Loss - {:.4f}'.format(idx+1, total_loss_train, total_loss_valid))

    sess.close()


if __name__ == "__main__":
    train()
