from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers import GlobalMaxPooling2D
from keras.layers import GlobalAveragePooling2D
from keras.layers import GaussianNoise
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.layers.core import Lambda
from keras.layers import UpSampling2D
from keras.layers import concatenate
from keras.layers import Input
from keras.models import Model
from keras import regularizers
from keras import losses
from keras import backend as K
from keras.backend import tf
import numpy as np
from keras.layers.convolutional import SeparableConv2D
from keras.layers.convolutional import Conv2DTranspose
from keras.layers.merge import Add

from src.models.diag_conv_2d import DiagConv2D


def conv_single(filter_count, conv_size, inputs):
    conv = Conv2D(filter_count, conv_size, padding='same', dilation_rate=(1, 1))(inputs)
    conv = Activation('relu')(conv)
    conv = BatchNormalization()(conv)

    return conv


def conv_double(filter_count, conv_size, inputs):
    conv = Conv2D(filter_count, conv_size, padding='same', dilation_rate=(1, 1))(inputs)
    conv = Activation('relu')(conv)
    conv = BatchNormalization()(conv)
    conv = Conv2D(filter_count, conv_size, padding='same', dilation_rate=(1, 1))(conv)
    conv = Activation('relu')(conv)
    conv = BatchNormalization()(conv)

    return conv


def diag_conv_single(filter_count, band_width, band_length, orientation, inputs):
    conv = DiagConv2D(filter_count, band_width, band_length, orientation, padding='same', dilation_rate=(1, 1))(inputs)
    conv = Activation('relu')(conv)
    conv = BatchNormalization()(conv)

    return conv


def conv_single_fcn(filter_count, conv_size, strides, inputs):
    conv = Conv2D(filter_count, conv_size, strides=strides, padding='same', dilation_rate=(1, 1))(inputs)
    conv = BatchNormalization()(conv)
    conv = Activation('relu')(conv)

    return conv


def mnet_layer(filter_count, conv_size, strides, inputs):
    conv = SeparableConv2D(filter_count, conv_size, strides=strides, padding='same')(inputs)
    conv = BatchNormalization()(conv)
    conv = Activation('relu')(conv)

    return conv


def pool(inputs):
    return MaxPooling2D(pool_size=(2, 2))(inputs)


def upsample(inputs):
    return UpSampling2D(size=(2, 2))(inputs)


def concat(inputs):
    return concatenate(inputs, axis=3)


def dropout(drop_rate, inputs):
    return Dropout(rate=drop_rate)(inputs)