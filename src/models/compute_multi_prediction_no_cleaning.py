import os

from keras.backend import tf
from keras.models import load_model
import numpy as np

from src.models.diag_conv_2d import DiagConv2D
from src.libs import image_utils


def predict(regular_model, images, batch_size):
    predicted = regular_model.predict(images, batch_size=batch_size)
    predicted = (np.array(predicted) > 0.5).astype(float)

    return predicted


BATCH_SIZE = 2
EXTEND_BORDERS = False # needed for validation of mnet

REGULAR_MODEL_DIR = 'models/test'
REGULAR_MODEL_NAME = 'extended_unet_epochs_60.h5'

IMAGE_DIR = 'data/training/preprocessed_binary_128_flipped_all/images'
IMAGE_PREFIX = 'satImage'
PREDICTION_DIR = 'out/predicted'
PREDICTION_PREFIX = 'prediction'


### load models
regular_model = load_model(os.path.join(REGULAR_MODEL_DIR, REGULAR_MODEL_NAME), custom_objects={
    'tf': tf,
    'DiagConv2D': DiagConv2D
})


### load, preprocess, predict and merge images
images_0_pd = image_utils.load_images(IMAGE_DIR, IMAGE_PREFIX)
images_0 = image_utils.to_raw_array(images_0_pd)

if EXTEND_BORDERS:
    images_0 = image_utils.extend_borders(images_0, 8)

images_90 = image_utils.rotate_90(images_0, 1)
images_180 = image_utils.rotate_90(images_0, 2)
images_270 = image_utils.rotate_90(images_0, 3)
images_0_flip = image_utils.apply_flip(images_0, (True, False))
images_90_flip = image_utils.apply_flip(images_90, (False, True))
images_180_flip = image_utils.apply_flip(images_180, (True, False))
images_270_flip = image_utils.apply_flip(images_270, (False, True))

prediction_0 = predict(regular_model, images_0, BATCH_SIZE)
prediction_90 = predict(regular_model, images_90, BATCH_SIZE)
prediction_180 = predict(regular_model, images_180, BATCH_SIZE)
prediction_270 = predict(regular_model, images_270, BATCH_SIZE)
prediction_0_flip = predict(regular_model, images_0_flip, BATCH_SIZE)
prediction_90_flip = predict(regular_model, images_90_flip, BATCH_SIZE)
prediction_180_flip = predict(regular_model, images_180_flip, BATCH_SIZE)
prediction_270_flip = predict(regular_model, images_270_flip, BATCH_SIZE)

prediction_90 = image_utils.rotate_90(prediction_90, 3)

prediction_180 = image_utils.rotate_90(prediction_180, 2)

prediction_270 = image_utils.rotate_90(prediction_270, 1)

prediction_0_flip = image_utils.apply_flip(prediction_0_flip, (True, False))

prediction_90_flip = image_utils.apply_flip(prediction_90_flip, (False, True))
prediction_90_flip = image_utils.rotate_90(prediction_90_flip, 3)

prediction_180_flip = image_utils.apply_flip(prediction_180_flip, (True, False))
prediction_180_flip = image_utils.rotate_90(prediction_180_flip, 2)

prediction_270_flip = image_utils.apply_flip(prediction_270_flip, (False, True))
prediction_270_flip = image_utils.rotate_90(prediction_270_flip, 1)

merged_images = \
    prediction_0 + prediction_90 + prediction_180 + \
    prediction_270 + prediction_0_flip + prediction_90_flip + \
    prediction_180_flip + prediction_270_flip

merged_images = np.clip(merged_images, 0.0, 1.0)

if EXTEND_BORDERS:
    merged_images = image_utils.crop_borders(merged_images, 8)


### save predictions
merged_images = np.tile(merged_images, (1, 1, 3))
image_ids = list(images_0_pd['id'])

image_utils.store_images(PREDICTION_DIR, PREDICTION_PREFIX, merged_images, 'RGB', ids=image_ids)
