import os

from keras import losses
from keras import optimizers
import numpy as np

from src.libs import image_utils
import src.models.model_extended_unet_cleaning as cnn


ROUNDS = 6
EPOCHS = 5
BATCH_SIZE = 6

MODEL_DIR = 'out/models/extended_unet_cleaning'
MODEL_NAME = 'model'

TRAINING_IMAGE_DIR = 'data/training/preprocessed_binary_256_flipped_all_corrupted_split/training/images'
TRAINING_IMAGE_PREFIX = 'satImage'
TRAINING_CORRUPTED_IMAGE_DIR = 'data/training/preprocessed_binary_256_flipped_all_corrupted_split/training/corrupted'
TRAINING_CORRUPTED_IMAGE_PREFIX = 'satImage'
GT_IMAGE_DIR = 'data/training/preprocessed_binary_256_flipped_all_corrupted_split/training/groundtruth'
GT_IMAGE_PREFIX = 'satImage'


### load data
training_images = image_utils.load_images(TRAINING_IMAGE_DIR, TRAINING_IMAGE_PREFIX)
training_patches = image_utils.to_raw_array(training_images)

training_corrupted_images = image_utils.load_images(TRAINING_CORRUPTED_IMAGE_DIR, TRAINING_CORRUPTED_IMAGE_PREFIX)
training_corrupted_patches = image_utils.to_raw_array(training_corrupted_images)

training_corrupted_patches = np.expand_dims(training_corrupted_patches, axis=-1)
training_patches = np.concatenate([training_patches, training_corrupted_patches], axis=-1)

gt_images = image_utils.load_images(GT_IMAGE_DIR, GT_IMAGE_PREFIX)
labels = image_utils.to_raw_array(gt_images)

labels = np.expand_dims(labels, axis=-1)

print(training_patches.shape)
print(labels.shape)


### fit model
input_shape = (None, None, training_patches.shape[3])

model = cnn.build_model(input_shape)
optimizer = optimizers.Adam()
loss = losses.mean_absolute_error

model.compile(optimizer=optimizer, loss=loss, metrics=["accuracy"])

for i in range(0, ROUNDS):
    model.fit(training_patches, labels, epochs=EPOCHS, batch_size=BATCH_SIZE)

    try:
        os.makedirs(MODEL_DIR)
    except FileExistsError:
        pass

    model.save(os.path.join(MODEL_DIR, MODEL_NAME + '_' + str((i + 1) * EPOCHS) + '.h5'))
