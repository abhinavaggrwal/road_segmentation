from keras.layers.core import Activation
from keras.layers import Input
from keras.models import Model
from keras.layers.convolutional import Conv2DTranspose
from keras.layers.merge import Add

from src.models.model_utils import conv_single_fcn
from src.models.model_utils import mnet_layer


def build_model(input_shape, upsampling):
    inputs = Input(input_shape)

    conv1 = conv_single_fcn(32, (3, 3), (2,2), inputs)

    conv2 = mnet_layer(64, (3,3), (1,1), conv1)

    conv3_1 = mnet_layer(128, (3,3), (2,2), conv2)
    conv3_2 = mnet_layer(128, (3,3), (1,1), conv3_1)

    conv4_1 = mnet_layer(256, (3,3), (2,2), conv3_2)
    conv4_2 = mnet_layer(256, (3,3), (1,1), conv4_1)

    conv5_1 = mnet_layer(512, (3,3), (2,2), conv4_2)
    conv5_2 = mnet_layer(512, (3,3), (1,1), conv5_1)
    conv5_3 = mnet_layer(512, (3,3), (1,1), conv5_2)
    conv5_4 = mnet_layer(512, (3,3), (1,1), conv5_3)
    conv5_5 = mnet_layer(512, (3,3), (1,1), conv5_4)
    conv5_6 = mnet_layer(512, (3,3), (1,1), conv5_5)

    conv6_1 = mnet_layer(1024, (3,3), (2,2), conv5_6)
    conv6_2 = mnet_layer(1024, (3,3), (1,1), conv6_1)

    conv7 = conv_single_fcn(2, (3,3), (1,1), conv6_2)

    if upsampling=='single':
        deconv = Conv2DTranspose(1, (64, 64), strides=(32,32), padding='same')(conv7)

        outputs = Activation('sigmoid')(deconv)

    if upsampling=='two':
        deconv1 = Conv2DTranspose(512, (4, 4), strides=(2,2), padding='same')(conv7)
        fuse1 = Add()([deconv1, conv5_1])

        deconv2 = Conv2DTranspose(1, (32, 32), strides=(16,16), padding='same')(fuse1)

        outputs = Activation('sigmoid')(deconv2)

    if upsampling=='three':
        deconv1 = Conv2DTranspose(512, (4, 4), strides=(2,2), padding='same')(conv7)
        fuse1 = Add()([deconv1, conv5_1])

        deconv2 = Conv2DTranspose(256, (4, 4), strides=(2,2), padding='same')(fuse1)
        fuse2 = Add()([deconv2, conv4_1])

        deconv3 = Conv2DTranspose(1, (16, 16), strides=(8,8), padding='same')(fuse2)

        outputs = Activation('sigmoid')(deconv3)

    return Model(inputs=inputs, outputs=outputs)
