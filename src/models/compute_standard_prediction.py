import os

from keras.backend import tf
from keras.models import load_model
import numpy as np

from src.libs import image_utils
from src.models.diag_conv_2d import DiagConv2D


def clean_predict(regular_model, cleaning_model, images, batch_size):
    predicted = regular_model.predict(images, batch_size=batch_size)
    predicted = (np.array(predicted) > 0.5).astype(float)

    cleaning_input = np.concatenate([images, predicted], axis=-1)

    predicted = cleaning_model.predict(cleaning_input, batch_size=batch_size)
    predicted = (np.array(predicted) > 0.5).astype(float)

    return predicted


BATCH_SIZE = 2
EXTEND_BORDERS = False # needed for validation of mnet

REGULAR_MODEL_DIR = 'models/test'
REGULAR_MODEL_NAME = 'extended_unet_epochs_60.h5'
CLEANING_MODEL_DIR = 'models/test'
CLEANING_MODEL_NAME = 'extended_unet_cleaning_epochs_20.h5'

IMAGE_DIR = 'data/training/preprocessed_binary_128_flipped_all/images'
IMAGE_PREFIX = 'satImage'
PREDICTION_DIR = 'out/predicted'
PREDICTION_PREFIX = 'prediction'


### load model
regular_model = load_model(os.path.join(REGULAR_MODEL_DIR, REGULAR_MODEL_NAME), custom_objects={
    'tf': tf,
    'DiagConv2D': DiagConv2D
})

cleaning_model = load_model(os.path.join(CLEANING_MODEL_DIR, CLEANING_MODEL_NAME), custom_objects={
    'tf': tf,
    'DiagConv2D': DiagConv2D
})


### prediction
images_pd = image_utils.load_images(IMAGE_DIR, IMAGE_PREFIX)
images = image_utils.to_raw_array(images_pd)

if EXTEND_BORDERS:
    images = image_utils.extend_borders(images, 8)

prediction = clean_predict(regular_model, cleaning_model, images, BATCH_SIZE)

if EXTEND_BORDERS:
    prediction = image_utils.crop_borders(prediction, 8)


### save predictions
prediction = np.tile(prediction, (1, 1, 3))
image_ids = list(images_pd['id'])

image_utils.store_images(PREDICTION_DIR, PREDICTION_PREFIX, prediction, 'RGB', ids=image_ids)
