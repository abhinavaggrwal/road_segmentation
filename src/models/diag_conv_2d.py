from keras.layers.convolutional import Conv2D
from keras.utils import conv_utils
from keras import backend as K


class DiagConv2D(Conv2D):
    def __init__(self,
                filters,
                band_width,
                band_length,
                orientation='td',
                strides=(1, 1),
                padding='valid',
                data_format=None,
                dilation_rate=(1, 1),
                activation=None,
                use_bias=True,
                kernel_initializer='glorot_uniform',
                bias_initializer='zeros',
                kernel_regularizer=None,
                bias_regularizer=None,
                activity_regularizer=None,
                kernel_constraint=None,
                bias_constraint=None,
                **kwargs):
        super(DiagConv2D, self).__init__(
            filters=filters,
            kernel_size=(band_width, band_length),
            strides=strides,
            padding=padding,
            data_format=data_format,
            dilation_rate=dilation_rate,
            activation=activation,
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            bias_initializer=bias_initializer,
            kernel_regularizer=kernel_regularizer,
            bias_regularizer=bias_regularizer,
            activity_regularizer=activity_regularizer,
            kernel_constraint=kernel_constraint,
            bias_constraint=bias_constraint,
            **kwargs)

        self.band_width = band_width
        self.band_length = band_length
        self.orientation = orientation


    def call(self, inputs):
        half_size = (self.band_width // 2) + 1

        input_dim = self.kernel.shape[2].value
        output_dim = self.kernel.shape[3].value

        v_parts = []

        if self.orientation == 'td':
            for j in range(0, self.band_length):
                h_parts = []

                if j > half_size - 1:
                    h_parts.append(K.zeros((j - half_size + 1, 1, input_dim, output_dim), dtype='float32'))

                i_s = max(0, j - half_size + 1)
                i_e = min(self.band_length, j + half_size)

                h_parts.append(self.kernel[0:(i_e - i_s), j:j+1])

                if j < self.band_length - half_size:
                    h_parts.append(K.zeros((self.band_length - half_size - j, 1, input_dim, output_dim), dtype='float32'))

                v_parts.append(K.concatenate(h_parts, axis=0))
        if self.orientation == 'dt':
            for j in range(0, self.band_length):
                h_parts = []

                if j < self.band_length - half_size:
                    h_parts.append(K.zeros((self.band_length - half_size - j, 1, input_dim, output_dim), dtype='float32'))

                i_s = max(0, self.band_length - half_size - j)
                i_e = min(self.band_length, self.band_length + half_size - j - 1)

                h_parts.append(self.kernel[0:(i_e - i_s), j:j+1])

                if j > half_size - 1:
                    h_parts.append(K.zeros((j - half_size + 1, 1, input_dim, output_dim), dtype='float32'))

                v_parts.append(K.concatenate(h_parts, axis=0))

        kernel = K.concatenate(v_parts, axis=1)

        outputs = K.conv2d(
            inputs,
            kernel,
            strides=self.strides,
            padding=self.padding,
            data_format=self.data_format,
            dilation_rate=self.dilation_rate)

        if self.use_bias:
            outputs = K.bias_add(
                outputs,
                self.bias,
                data_format=self.data_format)

        if self.activation is not None:
            return self.activation(outputs)

        return outputs


    def compute_output_shape(self, input_shape):
        if self.data_format == 'channels_last':
            space = input_shape[1:-1]
            new_space = []
            for i in range(len(space)):
                new_dim = conv_utils.conv_output_length(
                    space[i],
                    self.band_length,
                    padding=self.padding,
                    stride=self.strides[i],
                    dilation=self.dilation_rate[i])
                new_space.append(new_dim)

            return (input_shape[0],) + tuple(new_space) + (self.filters,)
        if self.data_format == 'channels_first':
            space = input_shape[2:]
            new_space = []
            for i in range(len(space)):
                new_dim = conv_utils.conv_output_length(
                    space[i],
                    self.band_length,
                    padding=self.padding,
                    stride=self.strides[i],
                    dilation=self.dilation_rate[i])
                new_space.append(new_dim)

            return (input_shape[0], self.filters) + tuple(new_space)


    def get_config(self):
        config = {
            'band_width': self.band_width,
            'band_length': self.band_length,
            'orientation': self.orientation
            }

        base_config = super(DiagConv2D, self).get_config()
        base_config.pop('kernel_size')

        return dict(list(base_config.items()) + list(config.items()))
