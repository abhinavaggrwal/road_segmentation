from keras.layers.core import Activation
from keras.layers import Input
from keras.models import Model

from src.models.model_utils import diag_conv_single
from src.models.model_utils import conv_single
from src.models.model_utils import conv_double
from src.models.model_utils import pool
from src.models.model_utils import upsample
from src.models.model_utils import concat


def build_model(input_shape):
    inputs = Input(input_shape)

    conv_down_32 = conv_single(32, (5, 5), inputs)
    conv_down_32_l = conv_single(16, (21, 5), conv_down_32)
    conv_down_32_r = conv_single(16, (5, 21), conv_down_32)
    conv_down_32_td = diag_conv_single(16, 5, 21, 'td', conv_down_32)
    conv_down_32_dt = diag_conv_single(16, 5, 21, 'dt', conv_down_32)

    conv_down_32_c = concat([conv_down_32_l, conv_down_32_r, conv_down_32_td, conv_down_32_dt])

    pool_32 = pool(conv_down_32_c)
    conv_down_64 = conv_single(64, (5, 5), pool_32)
    conv_down_64_l = conv_single(32, (19, 5), conv_down_64)
    conv_down_64_r = conv_single(32, (5, 19), conv_down_64)
    conv_down_64_td = diag_conv_single(32, 5, 19, 'td', conv_down_64)
    conv_down_64_dt = diag_conv_single(32, 5, 19, 'dt', conv_down_64)

    conv_down_64_c = concat([conv_down_64_l, conv_down_64_r, conv_down_64_td, conv_down_64_dt])

    pool_64 = pool(conv_down_64_c)
    conv_down_128 = conv_single(128, (5, 5), pool_64)
    conv_down_128_l = conv_single(64, (17, 5), conv_down_128)
    conv_down_128_r = conv_single(64, (5, 17), conv_down_128)
    conv_down_128_td = diag_conv_single(64, 5, 17, 'td', conv_down_128)
    conv_down_128_dt = diag_conv_single(64, 5, 17, 'dt', conv_down_128)

    conv_down_128_c = concat([conv_down_128_l, conv_down_128_r, conv_down_128_td, conv_down_128_dt])

    pool_128 = pool(conv_down_128_c)
    conv_down_256 = conv_single(256, (5, 5), pool_128)
    conv_down_256_l = conv_single(128, (15, 5), conv_down_256)
    conv_down_256_r = conv_single(128, (5, 15), conv_down_256)
    conv_down_256_td = diag_conv_single(128, 5, 15, 'td', conv_down_256)
    conv_down_256_dt = diag_conv_single(128, 5, 15, 'dt', conv_down_256)

    conv_down_256_c = concat([conv_down_256_l, conv_down_256_r, conv_down_256_td, conv_down_256_dt])

    pool_256 = pool(conv_down_256_c)
    conv_512 = conv_double(512, (5, 5), pool_256)

    upsamp_512 = upsample(conv_512)

    conv_256 = conv_single(256, (5, 5), upsamp_512)
    concat_256_l = concat([conv_down_256_l, conv_256])
    concat_256_r = concat([conv_down_256_r, conv_256])
    concat_256_td = concat([conv_down_256_td, conv_256])
    concat_256_dt = concat([conv_down_256_dt, conv_256])
    conv_up_256_l = conv_single(128, (15, 5), concat_256_l)
    conv_up_256_r = conv_single(128, (5, 15), concat_256_r)
    conv_up_256_td = diag_conv_single(128, 5, 15, 'td', concat_256_td)
    conv_up_256_dt = diag_conv_single(128, 5, 15, 'dt', concat_256_dt)
    conv_up_256_c = concat([conv_up_256_l, conv_up_256_r, conv_up_256_td, conv_up_256_dt])
    conv_up_256 = conv_single(256, (5, 5), conv_up_256_c)

    upsamp_256 = upsample(conv_up_256)

    conv_128 = conv_single(128, (5, 5), upsamp_256)
    concat_128_l = concat([conv_down_128_l, conv_128])
    concat_128_r = concat([conv_down_128_r, conv_128])
    concat_128_td = concat([conv_down_128_td, conv_128])
    concat_128_dt = concat([conv_down_128_dt, conv_128])
    conv_up_128_l = conv_single(64, (17, 5), concat_128_l)
    conv_up_128_r = conv_single(64, (5, 17), concat_128_r)
    conv_up_128_td = diag_conv_single(64, 5, 17, 'td', concat_128_td)
    conv_up_128_dt = diag_conv_single(64, 5, 17, 'dt', concat_128_dt)
    conv_up_128_c = concat([conv_up_128_l, conv_up_128_r, conv_up_128_td, conv_up_128_dt])
    conv_up_128 = conv_single(128, (5, 5), conv_up_128_c)

    upsamp_128 = upsample(conv_up_128)

    conv_64 = conv_single(64, (5, 5), upsamp_128)
    concat_64_l = concat([conv_down_64_l, conv_64])
    concat_64_r = concat([conv_down_64_r, conv_64])
    concat_64_td = concat([conv_down_64_td, conv_64])
    concat_64_dt = concat([conv_down_64_dt, conv_64])
    conv_up_64_l = conv_single(32, (19, 5), concat_64_l)
    conv_up_64_r = conv_single(32, (5, 19), concat_64_r)
    conv_up_64_td = diag_conv_single(32, 5, 19, 'td', concat_64_td)
    conv_up_64_dt = diag_conv_single(32, 5, 19, 'dt', concat_64_dt)
    conv_up_64_c = concat([conv_up_64_l, conv_up_64_r, conv_up_64_td, conv_up_64_dt])
    conv_up_64 = conv_single(64, (5, 5), conv_up_64_c)

    upsamp_64 = upsample(conv_up_64)

    conv_32 = conv_single(32, (5, 5), upsamp_64)
    concat_32_l = concat([conv_down_32_l, conv_32])
    concat_32_r = concat([conv_down_32_r, conv_32])
    concat_32_dt = concat([conv_down_32_dt, conv_32])
    concat_32_td = concat([conv_down_32_td, conv_32])
    conv_up_32_l = conv_single(16, (21, 5), concat_32_l)
    conv_up_32_r = conv_single(16, (5, 21), concat_32_r)
    conv_up_32_td = diag_conv_single(16, 5, 21, 'td', concat_32_td)
    conv_up_32_dt = diag_conv_single(16, 5, 21, 'dt', concat_32_dt)
    conv_up_32_c = concat([conv_up_32_l, conv_up_32_r, conv_up_32_td, conv_up_32_dt])
    conv_up_32 = conv_single(32, (5, 5), conv_up_32_c)

    conv_1 = conv_single(1, (5, 5), conv_up_32)

    outputs = Activation('sigmoid')(conv_1)

    return Model(inputs=inputs, outputs=outputs)
