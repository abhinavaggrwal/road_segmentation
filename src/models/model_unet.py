from keras.layers.core import Activation
from keras.layers import Input
from keras.models import Model

from src.models.model_utils import conv_single
from src.models.model_utils import conv_double
from src.models.model_utils import pool
from src.models.model_utils import upsample
from src.models.model_utils import concat


def build_model(input_shape):
    inputs = Input(input_shape)

    conv_down_64 = conv_double(32, (5, 5), inputs)
    pool_64 = pool(conv_down_64)
    conv_down_128 = conv_double(64, (5, 5), pool_64)
    pool_128 = pool(conv_down_128)
    conv_down_256 = conv_double(128, (5, 5), pool_128)
    pool_256 = pool(conv_down_256)
    conv_down_512 = conv_double(256, (5, 5), pool_256)
    pool_512 = pool(conv_down_512)

    conv_1024 = conv_double(512, (5, 5), pool_512)

    upsamp_1024 = upsample(conv_1024)

    conv_512 = conv_single(256, (5, 5), upsamp_1024)
    concat_512 = concat([conv_down_512, conv_512])
    conv_up_512 = conv_double(256, (5, 5), concat_512)

    upsamp_512 = upsample(conv_up_512)

    conv_256 = conv_single(128, (5, 5), upsamp_512)
    concat_256 = concat([conv_down_256, conv_256])
    conv_up_256 = conv_double(128, (5, 5), concat_256)

    upsamp_256 = upsample(conv_up_256)

    conv_128 = conv_single(64, (5, 5), upsamp_256)
    concat_128 = concat([conv_down_128, conv_128])
    conv_up_128 = conv_double(64, (5, 5), concat_128)

    upsamp_128 = upsample(conv_up_128)

    conv_64 = conv_single(32, (5, 5), upsamp_128)
    concat_64 = concat([conv_down_64, conv_64])
    conv_up_64 = conv_double(32, (5, 5), concat_64)

    conv_1 = conv_single(1, (5, 5), conv_up_64)

    outputs = Activation('sigmoid')(conv_1)

    return Model(inputs=inputs, outputs=outputs)