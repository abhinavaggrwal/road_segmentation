import os

from keras import losses
from keras import optimizers
import numpy as np

from src.libs import image_utils
import src.models.model_unet as cnn


ROUNDS = 16
EPOCHS = 5
BATCH_SIZE = 30

MODEL_DIR = 'out/models/unet'
MODEL_NAME = 'model'

TRAINING_IMAGE_DIR = 'data/training/preprocessed_binary_128_flipped_all_split/training/images'
TRAINING_IMAGE_PREFIX = 'satImage'
GT_IMAGE_DIR = 'data/training/preprocessed_binary_128_flipped_all_split/training/groundtruth'
GT_IMAGE_PREFIX = 'satImage'


### load data
training_images = image_utils.load_images(TRAINING_IMAGE_DIR, TRAINING_IMAGE_PREFIX)
training_patches = image_utils.to_raw_array(training_images)

gt_images = image_utils.load_images(GT_IMAGE_DIR, GT_IMAGE_PREFIX)
labels = image_utils.to_raw_array(gt_images)

labels = np.expand_dims(labels, axis=-1)

print(training_patches.shape)
print(labels.shape)


### fit model
input_shape = (None, None, training_patches.shape[3])

model = cnn.build_model(input_shape)
optimizer = optimizers.Adam()
loss = losses.mean_absolute_error

model.compile(optimizer=optimizer, loss=loss, metrics=["accuracy"])

for i in range(0, ROUNDS):
    model.fit(training_patches, labels, epochs=EPOCHS, batch_size=BATCH_SIZE)

    try:
        os.makedirs(MODEL_DIR)
    except FileExistsError:
        pass

    model.save(os.path.join(MODEL_DIR, MODEL_NAME + '_' + str((i + 1) * EPOCHS) + '.h5'))
