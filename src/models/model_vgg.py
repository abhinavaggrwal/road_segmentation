from keras.layers.core import Activation
from keras.layers import Input
from keras.models import Model
from keras.layers.convolutional import Conv2DTranspose
from keras.layers.merge import Add

from src.models.model_utils import conv_single_fcn
from src.models.model_utils import pool
from src.models.model_utils import dropout


def build_model(input_shape, upsampling, drop_rate=0.5):
    inputs = Input(input_shape)

    conv1_1 = conv_single_fcn(64, (3, 3), (1,1), inputs)
    conv1_2 = conv_single_fcn(64, (3, 3), (1,1), conv1_1)
    pool1 = pool(conv1_2)

    conv2_1 = conv_single_fcn(128, (3,3), (1,1), pool1)
    conv2_2 = conv_single_fcn(128, (3,3), (1,1), conv2_1)
    pool2 = pool(conv2_2)

    conv3_1 = conv_single_fcn(256, (3,3), (1,1), pool2)
    conv3_2 = conv_single_fcn(256, (3,3), (1,1), conv3_1)
    conv3_3 = conv_single_fcn(256, (3,3), (1,1), conv3_2)
    pool3 = pool(conv3_3)

    conv4_1 = conv_single_fcn(512, (3,3), (1,1), pool3)
    conv4_2 = conv_single_fcn(512, (3,3), (1,1), conv4_1)
    conv4_3 = conv_single_fcn(512, (3,3), (1,1), conv4_2)
    pool4 = pool(conv4_3)

    conv5_1 = conv_single_fcn(512, (3,3), (1,1), pool4)
    conv5_2 = conv_single_fcn(512, (3,3), (1,1), conv5_1)
    conv5_3 = conv_single_fcn(512, (3,3), (1,1), conv5_2)
    pool5 = pool(conv5_3)

    conv6 = conv_single_fcn(1024, (8,8), (1,1), pool5)
    conv6_drop = dropout(drop_rate, conv6)

    conv7 = conv_single_fcn(1024, (1,1), (1,1), conv6_drop)
    conv7_drop = dropout(drop_rate, conv7)

    fcn8 = conv_single_fcn(2, (1,1), (1,1), conv7_drop)

    if upsampling=='single':
        deconv = Conv2DTranspose(1, (64, 64), strides=(32,32), padding='same')(fcn8)

        outputs = Activation('sigmoid')(deconv)

    if upsampling=='two':
        deconv1 = Conv2DTranspose(512, (4, 4), strides=(2,2), padding='same')(fcn8)
        fuse1 = Add()([deconv1, pool4])

        deconv2 = Conv2DTranspose(1, (32, 32), strides=(16,16), padding='same')(fuse1)

        outputs = Activation('sigmoid')(deconv2)

    if upsampling=='three':
        deconv1 = Conv2DTranspose(512, (4, 4), strides=(2,2), padding='same')(fcn8)
        fuse1 = Add()([deconv1, pool4])

        deconv2 = Conv2DTranspose(256, (4, 4), strides=(2,2), padding='same')(fuse1)
        fuse2 = Add()([deconv2, pool3])

        deconv3 = Conv2DTranspose(1, (16, 16), strides=(8,8), padding='same')(fuse2)

        outputs = Activation('sigmoid')(deconv3)

    return Model(inputs=inputs, outputs=outputs)