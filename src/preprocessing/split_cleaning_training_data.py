import random
random.seed(2)

import numpy as np
np.random.seed(seed=2)

import os

from PIL import Image

from src.libs import image_utils


GT_IMAGE_VALUE_THRESHOLD = 0.8

# This is just for offset calculation.
# Should be the same as the values in 'generate_cleaning_training_data.py'
NORMAL_PATCH_COUNT = 3
ROTATION_PATCH_COUNT = 1
CORRUPTION_COUNT = 3
FLIP_COUNT = 4

VALIDATION_RATIO = 30 / 100

IMAGE_DIR = 'data/training/preprocessed_binary_256_flipped_all_corrupted/images'
IMAGE_PREFIX = 'satImage'
CORRUPTED_GT_IMAGE_DIR = 'data/training/preprocessed_binary_256_flipped_all_corrupted/corrupted'
CORRUPTED_GT_IMAGE_PREFIX = 'satImage'
GT_IMAGE_DIR = 'data/training/preprocessed_binary_256_flipped_all_corrupted/groundtruth'
GT_IMAGE_PREFIX = 'satImage'
ORIGINAL_IMAGE_DIR = 'data/training/original/images'
ORIGINAL_IMAGE_PREFIX = 'satImage'
ORIGINAL_GT_IMAGE_DIR = 'data/training/original/groundtruth'
ORIGINAL_GT_IMAGE_PREFIX = 'satImage'
TRAINING_IMAGE_OUT_DIR = 'out/preprocessed/training/images'
TRAINING_CORRUPTED_GT_IMAGE_OUT_DIR = 'out/preprocessed/training/corrupted'
TRAINING_GT_IMAGE_OUT_DIR = 'out/preprocessed/training/groundtruth'
VALIDATION_IMAGE_OUT_DIR = 'out/preprocessed/validation/images'
VALIDATION_GT_IMAGE_OUT_DIR = 'out/preprocessed/validation/groundtruth'


### load images
images = image_utils.load_images(IMAGE_DIR, IMAGE_PREFIX)
images = image_utils.to_raw_array(images)

corrupted_gt_images = image_utils.load_images(CORRUPTED_GT_IMAGE_DIR, CORRUPTED_GT_IMAGE_PREFIX)
corrupted_gt_images = image_utils.to_raw_array(corrupted_gt_images)

gt_images = image_utils.load_images(GT_IMAGE_DIR, GT_IMAGE_PREFIX)
gt_images = image_utils.to_raw_array(gt_images)

original_images = image_utils.load_images(ORIGINAL_IMAGE_DIR, ORIGINAL_IMAGE_PREFIX)
original_images = image_utils.to_raw_array(original_images)

original_gt_images = image_utils.load_images(ORIGINAL_GT_IMAGE_DIR, ORIGINAL_GT_IMAGE_PREFIX)
original_gt_images = image_utils.to_raw_array(original_gt_images)


### convert original gt image values to binary
original_gt_images = (original_gt_images > GT_IMAGE_VALUE_THRESHOLD).astype(float)


### compute random indices
total_image_count = int(images.shape[0] / ((NORMAL_PATCH_COUNT + ROTATION_PATCH_COUNT) * CORRUPTION_COUNT * FLIP_COUNT))

training_count = int(total_image_count * (1.0 - VALIDATION_RATIO))
validation_count = total_image_count - training_count

indices = np.random.permutation(total_image_count)
training_indices = indices[:training_count]
validation_indices = indices[training_count:]


### compute indices such that the final sets consist of patches from distinct source images
index_offsets = np.arange(NORMAL_PATCH_COUNT + ROTATION_PATCH_COUNT)

training_indices = np.repeat(training_indices, NORMAL_PATCH_COUNT + ROTATION_PATCH_COUNT)
training_indices *= NORMAL_PATCH_COUNT + ROTATION_PATCH_COUNT
training_indices += np.tile(index_offsets, training_count)

training_indices = np.tile(training_indices, FLIP_COUNT)

flip_repeat_offsets = np.arange(FLIP_COUNT) * total_image_count * (NORMAL_PATCH_COUNT + ROTATION_PATCH_COUNT)
flip_repeat_offsets = np.repeat(flip_repeat_offsets, training_count * (NORMAL_PATCH_COUNT + ROTATION_PATCH_COUNT))

training_indices += flip_repeat_offsets 

training_indices = np.tile(training_indices, CORRUPTION_COUNT)

corruption_repeat_offsets = np.arange(CORRUPTION_COUNT) * total_image_count * (NORMAL_PATCH_COUNT + ROTATION_PATCH_COUNT) * FLIP_COUNT
corruption_repeat_offsets = np.repeat(corruption_repeat_offsets, training_count * (NORMAL_PATCH_COUNT + ROTATION_PATCH_COUNT) * FLIP_COUNT)

training_indices += corruption_repeat_offsets 


### store images
training_images = images[training_indices]
validation_images = original_images[validation_indices]
training_corrupted_gt_images = corrupted_gt_images[training_indices]
training_gt_images = gt_images[training_indices]
validation_gt_images = original_gt_images[validation_indices]

image_utils.store_images(TRAINING_IMAGE_OUT_DIR, IMAGE_PREFIX, training_images, 'RGB', ids=training_indices)
image_utils.store_images(VALIDATION_IMAGE_OUT_DIR, IMAGE_PREFIX, validation_images, 'RGB', ids=validation_indices)
image_utils.store_images(TRAINING_CORRUPTED_GT_IMAGE_OUT_DIR, CORRUPTED_GT_IMAGE_PREFIX, training_corrupted_gt_images, 'L', ids=training_indices)
image_utils.store_images(TRAINING_GT_IMAGE_OUT_DIR, GT_IMAGE_PREFIX, training_gt_images, 'L', ids=training_indices)
image_utils.store_images(VALIDATION_GT_IMAGE_OUT_DIR, GT_IMAGE_PREFIX, validation_gt_images, 'L', ids=validation_indices)
