import random
random.seed(2)

import numpy as np
np.random.seed(seed=2)

import os

from PIL import Image

from src.libs import image_utils


GT_IMAGE_VALUE_THRESHOLD = 0.8
PATCH_SIZE = 256
BASE_ROTATION = 45
MAX_ROTATION_JITTER = 20

NORMAL_PATCH_COUNT = 3
ROTATION_PATCH_COUNT = 1
CORRUPTION_COUNT = 3

IMAGE_DIR = 'data/training/original/images'
IMAGE_PREFIX = 'satImage'
GT_IMAGE_DIR = 'data/training/original/groundtruth'
GT_IMAGE_PREFIX = 'satImage'
IMAGE_OUT_DIR = 'out/preprocessed/images'
CORRUPTED_GT_IMAGE_OUT_DIR = 'out/preprocessed/corrupted'
GT_IMAGE_OUT_DIR = 'out/preprocessed/groundtruth'


### load images
images = image_utils.load_images(IMAGE_DIR, IMAGE_PREFIX)
images = image_utils.to_raw_array(images)

gt_images = image_utils.load_images(GT_IMAGE_DIR, GT_IMAGE_PREFIX)
gt_images = image_utils.to_raw_array(gt_images)


### convert gt image values to binary
gt_images = (gt_images > GT_IMAGE_VALUE_THRESHOLD).astype(float)


### extract patches
patches = image_utils.extract_random_patches(
    images, NORMAL_PATCH_COUNT, ROTATION_PATCH_COUNT,
    PATCH_SIZE, BASE_ROTATION, MAX_ROTATION_JITTER, 3, 0)

gt_patches = image_utils.extract_random_patches(
    gt_images, NORMAL_PATCH_COUNT, ROTATION_PATCH_COUNT,
    PATCH_SIZE, BASE_ROTATION, MAX_ROTATION_JITTER, 0, 0)

patches_flip_1_0 = image_utils.apply_flip(patches, (True, False))
patches_flip_0_1 = image_utils.apply_flip(patches, (False, True))
patches_flip_1_1 = image_utils.apply_flip(patches, (True, True))

gt_patches_flip_1_0 = image_utils.apply_flip(gt_patches, (True, False))
gt_patches_flip_0_1 = image_utils.apply_flip(gt_patches, (False, True))
gt_patches_flip_1_1 = image_utils.apply_flip(gt_patches, (True, True))

patches = np.concatenate([
    patches,
    patches_flip_1_0,
    patches_flip_0_1,
    patches_flip_1_1], axis=0)

gt_patches = np.concatenate([
    gt_patches,
    gt_patches_flip_1_0,
    gt_patches_flip_0_1,
    gt_patches_flip_1_1], axis=0)


### generate corrupted gt images
patches_list = []
corrupted_gt_patches_list = []
gt_patches_list = []

for i in range(0, CORRUPTION_COUNT):
    patches_list.append(patches)

    corrupted_gt_patches = image_utils.apply_random_dilation(gt_patches)
    corrupted_gt_patches = image_utils.apply_random_gt_errors(corrupted_gt_patches)
    corrupted_gt_patches_list.append(corrupted_gt_patches)

    gt_patches_list.append(gt_patches)

patches = np.concatenate(patches_list, axis=0)
corrupted_gt_patches = np.concatenate(corrupted_gt_patches_list, axis=0)
gt_patches = np.concatenate(gt_patches_list, axis=0)


### store images
image_utils.store_images(IMAGE_OUT_DIR, IMAGE_PREFIX, patches, 'RGB')
image_utils.store_images(CORRUPTED_GT_IMAGE_OUT_DIR, IMAGE_PREFIX, corrupted_gt_patches, 'L')
image_utils.store_images(GT_IMAGE_OUT_DIR, GT_IMAGE_PREFIX, gt_patches, 'L')
