import numpy as np
from sklearn.metrics import f1_score


PREDICTION_PATH = 'submissions/validation/extended_unet_epochs_60_multi_prediction_cleaned.csv'
GT_PATH = 'submissions/validation_groundtruth.csv'


predictions = np.loadtxt(open(PREDICTION_PATH, 'rb'), delimiter=',', skiprows=1)
gt = np.loadtxt(open(GT_PATH, 'rb'), delimiter=',', skiprows=1)

if predictions.shape[0] == gt.shape[0]:
    score_correct_fraction = np.sum(gt[:, 1] == predictions[:, 1]) / predictions.shape[0]
    score_f1 = f1_score(gt[:, 1], predictions[:, 1])

    print(f'Fraction of correct values: {score_correct_fraction}')
    print(f'F1 score: {score_f1}')
else:
    print('Error: shapes do not match')
