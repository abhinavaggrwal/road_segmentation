import numpy as np

from src.libs import image_utils


OVERLAY_IMAGE_DIR = 'predictions/test/extended_unet_epochs_60_multi_prediction_cleaned'
OVERLAY_IMAGE_PREFIX = 'prediction'
BACKGROUND_IMAGE_DIR = 'data/test/original'
BACKGROUND_IMAGE_PREFIX = 'test'
OUT_IMAGE_DIR = 'out/overlay'
OUT_IMAGE_PREFIX = 'overlay'


overlay_images = image_utils.load_images(OVERLAY_IMAGE_DIR, OVERLAY_IMAGE_PREFIX)
overlay_patches = image_utils.to_raw_array(overlay_images)

background_images = image_utils.load_images(BACKGROUND_IMAGE_DIR, BACKGROUND_IMAGE_PREFIX)
background_patches = image_utils.to_raw_array(background_images)

overlaid = []

for i in range(0, background_patches.shape[0]):
    tmp = np.zeros((background_patches.shape[1], background_patches.shape[2], 3))
    tmp[:, :, 0] = background_patches[i, :, :, 0] * 0.7 + overlay_patches[i, :, :, 0] * 0.3
    tmp[:, :, 1] = background_patches[i, :, :, 1] * 0.7
    tmp[:, :, 2] = background_patches[i, :, :, 2] * 0.7

    overlaid.append(tmp)

overlaid = np.stack(overlaid, axis=0)

image_utils.store_images(OUT_IMAGE_DIR, OUT_IMAGE_PREFIX, overlaid, 'RGB')
