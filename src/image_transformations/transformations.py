#%%
import cv2
import os
import numpy as np
from tqdm import tqdm

#%%
img_path = '../../data/training/images'
gt_path = '../../data/training/groundtruth'

out_img_path = '../../data/training/trans_images'
out_gt_path = '../../data/training/trans_gt'

train_img = os.listdir(img_path)
train_gt = os.listdir(gt_path)

try:
    os.makedirs(out_img_path)
    os.makedirs(out_gt_path)
except FileExistsError:
    pass


#%%
def rot_trans(img_path, image, out_img_path, rot_angle, prefix):
    if not os.path.exists(os.path.join(out_img_path, prefix+image)):
        img = cv2.imread(os.path.join(img_path, image))
        cols, rows = img.shape[:2]

        M = cv2.getRotationMatrix2D((cols/2,rows/2),rot_angle,1)
        rot_img = cv2.warpAffine(img,M,(cols,rows))   

        cv2.imwrite(os.path.join(out_img_path, prefix+image), rot_img) 



#%%
for image, gth in tqdm(zip(train_img, train_gt)):

    rot_trans(img_path, image, out_img_path, 90,prefix='rot90_')
    rot_trans(img_path, image, out_img_path, rot_angle=45, prefix='rot45_')
    rot_trans(gt_path, gth, out_gt_path, 90, prefix='rot90_')
    rot_trans(gt_path, gth, out_gt_path, rot_angle=45, prefix='rot45_')

